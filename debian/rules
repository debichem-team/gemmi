#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

export PYBUILD_NAME = gemmi
export DEB_PYTHON_INSTALL_LAYOUT = deb

export DEB_CXXFLAGS_MAINT_APPEND = -fexcess-precision=fast # See #1042379

%:
	dh $@ --buildsystem cmake --with python3

# Shared library does not match Debian policy (See #1058868)
override_dh_auto_configure:
	dh_auto_configure -- -DBUILD_SHARED_LIBS=OFF -DUSE_PYTHON=ON

override_dh_auto_test:
	dh_auto_build -- check
	PYTHONPATH=$$(find . -name 'obj-*') pytest-3 -k 'not test_sfcalc_'

execute_after_dh_auto_clean:
	rm -rf .pytest_cache examples/__pycache__ tests/__pycache__

override_dh_installman: debian/gemmi.1
	dh_installman

debian/gemmi.1:
	help2man 'tail -n +9 docs/gemmi-help.txt' \
		--version-string $(DEB_VERSION_UPSTREAM) --name 'CLI for GEMMI library' \
		--no-info --help-option '' > $@
	sed -i 's/.TH GEMMI-HELP.TXT/.TH gemmi/' $@
	sed -i 's/gemmi-help.txt/gemmi/' $@
